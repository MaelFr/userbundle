<?php

namespace MaelFr\UserBundle;

use MaelFr\UserBundle\DependencyInjection\MaelFrUserExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MaelFrUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }

    public function getContainerExtension()
    {
        return new MaelFrUserExtension();
    }
}
