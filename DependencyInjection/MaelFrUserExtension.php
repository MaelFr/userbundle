<?php

namespace MaelFr\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class MaelFrUserExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    public function getAlias()
    {
        return 'maelfr_user';
    }

    public function prepend(ContainerBuilder $container)
    {
        // get all bundles
        $bundles = $container->getParameter('kernel.bundles');

        // Récupération de la configuration du bundle
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);

        // Configuration de FOSUserBundle
        if (isset($bundles['FOSUserBundle'])) {
            $container->prependExtensionConfig(
                'fos_user',
                [
                    'db_driver'     => $config['db_driver'],
                    'firewall_name' => $config['firewall_name'],
                    'user_class'    => $config['user_class'],
                    'registration'  => [
                        'form' => [
                            'type' => $config['registration']['form']['type'],
                        ],
                    ],
                    'profile' => [
                        'form' => [
                            'type' => $config['profile']['form']['type'],
                        ]
                    ]
                ]
            );
        }
    }
}
