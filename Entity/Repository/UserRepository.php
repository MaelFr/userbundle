<?php

namespace MaelFr\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use MaelFr\UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return integer
     */
    public function countByUsername(User $user)
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('u.usernameCanonical LIKE :username')
            ->setParameter('username', $user->getUsernameCanonical().'%')
            ->getQuery()
            ->getSingleScalarResult();
    }
}