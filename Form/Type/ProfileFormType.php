<?php

namespace MaelFr\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->add('firstname', TextType::class, [
                'label' => 'Prénom',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom',
            ])
//            ->add('iuf', IntegerType::class, [
//                'label'    => 'IUF',
//                'required' => false,
//            ])
//            ->add('roles', ChoiceType::class, [
//                'choices'      => $options['roles'],
//                'multiple'     => true,
//                'required'     => false,
//                'choice_label' => function ($value) {
//                    return substr($value, 5);
//                },
//            ])
//            ->add('enabled', CheckboxType::class, [
//                'label' => 'Actif',
//            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'maelfr_user_profile';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'roles' => [],
        ]);
    }
}
