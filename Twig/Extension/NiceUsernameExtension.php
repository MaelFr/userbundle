<?php

namespace MaelFr\UserBundle\Twig\Extension;

use MaelFr\UserBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

class NiceUsernameExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('niceUsername', [$this, 'niceUsername']),
        ];
    }

    public function niceUsername(UserInterface $user)
    {
        $username = $user->getUsername();

        if ($user instanceof User) {
            $username = $user->getFirstName() . ' ' . $user->getLastName();
        }

        return $username;
    }

    public function getName()
    {
        return 'nice_username_extension';
    }
}