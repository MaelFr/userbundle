<?php

namespace MaelFr\UserBundle\EventListener;

use MaelFr\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use MaelFr\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationListener implements EventSubscriberInterface
{
    /** @var UserManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
        ];
    }

    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        $formData = $event->getRequest()->get('fos_user_registration_form');
        $username = $formData['firstname'].' '.$formData['lastname'];

        /** @var User $user */
        $user = $event->getUser();
        $user->setUsername($username);

        // Vérification de l'unicité du username
        $nb = $this->userManager->countByUsername($user);

        if ($nb != 0) {
            $user->setUsername($username .' '. ($nb+1));
        }
    }
}