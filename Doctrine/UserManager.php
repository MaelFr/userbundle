<?php

namespace MaelFr\UserBundle\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager as BaseManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use MaelFr\UserBundle\Entity\User;

class UserManager extends BaseManager
{
    /** @var EntityManager */
    protected $em;

    public function __construct(
        PasswordUpdaterInterface $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        ObjectManager $om,
        $class,
        EntityManager $em
    ) {
        parent::__construct($passwordUpdater, $canonicalFieldsUpdater, $om, $class);
        $this->em = $em;
    }

    /**
     * Compte le nombre d'utilisateurs dont le username commence par celui de l'utilisateur fournit
     *
     * @param User $user
     * @return integer
     */
    public function countByUsername(User $user)
    {
        $this->updateCanonicalFields($user);

        return $this->repository->countByUsername($user);
    }

    /**
     * @param Integer $userId
     * @param Boolean $enabled
     * @throws \Exception
     */
    public function toggleEnabled($userId, $enabled)
    {
        try {
            /** @var User $user */
            $user = $this->repository->find($userId);
            $user->setEnabled($enabled);
            $this->updateUser($user);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
}