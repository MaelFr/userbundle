<?php

namespace MaelFr\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MaelFr\UserBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $u = new User();
        $u->setUsername('User');
        $u->setFirstName('UserFirstName');
        $u->setLastName('UserLastName');
        $u->setIuf(123456);
        $encoder = $this->container->get('security.password_encoder');
        $pwd = $encoder->encodePassword($u, '1234');
        $u->setPassword($pwd);
        $u->setEmail('user@appli.local');
        $u->setEnabled(true);
        $u->setRoles(['ROLE_ADHERENT']);

        $manager->persist($u);

        $b = new User();
        $b->setUsername('Bureau');
        $b->setFirstName('BureauFirstName');
        $b->setLastName('BureauLastName');
        $encoder = $this->container->get('security.password_encoder');
        $pwd = $encoder->encodePassword($b, '1234');
        $b->setPassword($pwd);
        $b->setEmail('bureau@appli.local');
        $b->setEnabled(true);
        $b->setRoles(['ROLE_BUREAU']);

        $manager->persist($b);

        $a = new User();
        $a->setUsername('Admin');
        $a->setFirstName('AdminFirstName');
        $a->setLastName('AdminLastName');
        $encoder = $this->container->get('security.password_encoder');
        $pwd = $encoder->encodePassword($a, '1234');
        $a->setPassword($pwd);
        $a->setEmail('admin@appli.local');
        $a->setEnabled(true);
        $a->setRoles(['ROLE_ADMIN']);

        $manager->persist($a);

        $manager->flush();

        $this->addReference('user', $u);
        $this->addReference('bureau', $b);
        $this->addReference('admin', $a);
    }

    public function getOrder()
    {
        return 1;
    }
}